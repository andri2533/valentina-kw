<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Hello World</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('easyui/theme/default/easyui.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('easyui/theme/icon.css') }}">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script type="text/javascript" src="{{ asset('easyui/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('easyui/jquery.easyui.min.js') }}"></script>
</head>
<body>
    <div class="easyui-layout" style="width:100%;height:620px;">
        <div data-options="region:'north',split:true,href:'l/top'" title="Server" style="height:75px; padding: 4px; overflow: hidden;" id="l_top">
        </div>
        <div data-options="region:'west',split:true,href:'l/left'" title="Database" style="width:150px; padding-top: 4px;" id="l_left"></div>
        <div data-options="region:'center',title:'Main Title',iconCls:'icon-ok',href:'l/middle'" id="l_middle"></div>
        @include('right')
        @include('bottom')
    </div>
    {{-- <script type="text/javascript" src="{{ asset('js/app.js') }}"></script> --}}
    {{-- <script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script> --}}
</body>
</html>