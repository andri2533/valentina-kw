@foreach($result AS $rw)
	<a 
	@if(@$rw->active==TRUE)
		style="color: green;"
	@endif
	onclick="updateServer('{{ json_encode($rw) }}')" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-server'"><b>{{ $rw->nama }}</b></a>
@endforeach
<a onclick="newServer()" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'"><b>Tambah Baru</b></a>
<div id="win_add_server" 
	class="easyui-window" title="Server Properties" 
	data-options="modal:true,iconCls:'icon-save',closed:true,collapsible:false,minimizable:false"
	style="width:370px;height:319px;padding-top:10px;padding-left:10px;padding-right:10px;">
	<form id="ff" method="post">
	    <div style="margin-bottom:20px">
		    <select class="easyui-combobox" name="connection_type" label="Type" data-options="panelHeight:50" style="width:100%">
		    	<option value="1">Standard TCP/IP</option>
		    	<option value="2">Standard TCP/IP With SSH</option>
		    </select>
	    </div>
	    <div style="margin-bottom:20px">
	        <input name="id" type="hidden" value="">
	        <input class="easyui-textbox" name="nama" type="text" style="width:100%" data-options="label:'Name',required:true">
	        {{ csrf_field() }}
	    </div>
	    <div style="margin-bottom:20px">
	        <input class="easyui-textbox" name="hosts" type="text" style="width:100%" data-options="label:'Host',required:true">
	    </div>
	    <div style="margin-bottom:20px">
	        <input class="easyui-textbox" name="users" type="text" style="width:100%" data-options="label:'User',required:true">
	    </div>
	    <div style="margin-bottom:20px">
	        <input class="easyui-textbox" name="password" type="password" style="width:100%" data-options="label:'Password'">
	    </div>
	    <center>
		    <button class="easyui-linkbutton" onclick="connectToServer()" type="button" data-options="iconCls:'fa fa-link'" style="width:80px;height:30px" id="connect">Connect</button>
		    <button class="easyui-linkbutton" type="submit" data-options="iconCls:'fa fa-save'" style="width:80px;height:30px">Save</button>
		    <a class="easyui-linkbutton" data-options="iconCls:'fa fa-close'" href="javascript:void(0)" onclick="javascript:$('#win_add_server').panel('close')" style="width:80px">Cancel</a>
		    <a class="easyui-linkbutton" data-options="iconCls:'fa fa-trash'" href="javascript:void(0)" onclick="removeServer()" style="width:80px">Remove</a>
	    </center>
	</form>

</div>
<script type="text/javascript">
	$('#ff').form({
	    url:'addServer',
	    onSubmit:function(){
	        return $(this).form('validate');
	    },
	    success:function(data){
	    	$("#l_top").panel('refresh');
	    	$('#win_add_server').panel('close');
	    	$(this).form('clear');
	    }
	});
	function newServer(){
    	$("#ff").form('clear');
    	$('#win_add_server').panel('open');
		$("input[name=_token]").val('{{ csrf_token() }}');
	}
	function updateServer(rw){
		var row = $.parseJSON(rw);
		$("#connect").show();
		if(row.active==true){
			$("#connect").hide();
		}
		$("input[name=_token]").val('{{ csrf_token() }}');
		$("#ff").form('load',row)
		$('#win_add_server').panel('open');
	}
	function removeServer(){
		var arr = $("#ff").serializeArray();
		$.post('removeServer?id='+arr[1].value, {_token:'{{ csrf_token() }}'}, function(data) {
	    	$("#l_top").panel('refresh');
	    	$('#win_add_server').panel('close');
	    	$(this).form('clear');
		});
	}
	function connectToServer(){
		$.post('connectToServer', $("#ff").serializeArray(), function(data, textStatus, xhr) {
			if(textStatus=='success'){
		    	$("#l_left").panel('refresh');
		    	$("#l_top").panel('refresh');
		    	$('#win_add_server').panel('close');
		    	$(this).form('clear');
			}
		});
	}
</script>