<table id="{!! implode('-', $db) !!}">
    <thead>
        <tr>
            @foreach($columns AS $cl)
                <th data-options="field:'{{ $cl->Field }}',sortable:true,formatter:formatValue,editor:{!! $cl->eu_editor !!}"
                    {{-- editor="{{ $cl->eu_editor }}" --}}
                    ><b>{{ $cl->Field }}</b></th>
            @endforeach
        </tr>
    </thead>
</table>
<div id="tbf"></div>
<script type="text/javascript" src="{{ asset('easyui/manual/datagrid-cellediting.js') }}"></script>
<script type="text/javascript">
    var table_id = "{!! implode('-', $db) !!}";
    var dg = $("#"+table_id).datagrid({
                url:'{!! Request::fullUrl() !!}&json=1',
                method:'get',
                border:true,
                singleSelect:true,
                fit:true,
                rowNumbers:true,
                pagination:true,
                footer:'#tbf',
                sortName:'{!! $columns[0]->Field !!}',
                onAfterEdit:(index, field, value)=>{
                    console.log(field);
                },
                onBeforeEdit:(index, row)=>{
                    console.log(index);
                    console.log(row);
                },
                // onDblClickCell:(index, field, value)=>{
                    // $(this).datagrid('beginEdit', index);
                    // var ed = $(this).datagrid('getEditor', {index:index,field:field});
                    // $(ed.target).focus();
                //     console.log($(this));
                // }
            }).datagrid('enableCellEditing');

    function formatValue(value, row, index){
        var val = value;
        if(isNaN(val) && val.length>50){ /*Jika bukan number && */
            return val.substring(0,50);
        }
        if(val==null){
            return "<i>&lt;null&gt;</i>";
        }

        return value;
    }
</script>