	<ul class="easyui-tree" id="tree">
    </ul>
    <div id="l_left_ms" class="easyui-menu" style="width:120px;">
        <div onclick="javascript:alert('new')">New Database</div>
        <div>
            <span>Open</span>
        </div>
        <div class="menu-sep"></div>
        <div data-options="iconCls:'fa fa-close'" onclick="disConnect()">Disconnect</div>
    </div>
    <div id="l_left_md" class="easyui-menu" style="width:120px;">
        <div onclick="javascript:alert('new')">New Table</div>
        <div>
            <span>Open</span>
        </div>
        <div class="menu-sep"></div>
        <div data-options="iconCls:'fa fa-trash'" onclick="removeDb()">Delete</div>
    </div>
    <div id="l_left_mt" class="easyui-menu" style="width:120px;">
        <div onclick="openTable()">
            <span>Open</span>
        </div>
        <div class="menu-sep"></div>
        <div data-options="iconCls:'fa fa-close'" onclick="removeTable()">Delete</div>
    </div>
<script type="text/javascript">
    $(function() {
        $("#tree").tree({
            url : '/l/left/tree',
            method : 'get',
            onClick:(v)=>{
                console.log(v);
            },
            onContextMenu: function(e, node){
                e.preventDefault();
                $('#tree').tree('select', node.target);
                if(node.type=='server'){
                    $('#l_left_ms').menu('show', {
                        left: e.pageX,
                        top: e.pageY
                    });
                }else if(node.type=='database'){
                    $('#l_left_md').menu('show', {
                        left: e.pageX,
                        top: e.pageY
                    });
                }else if(node.type=='table'){
                    $('#l_left_mt').menu('show', {
                        left: e.pageX,
                        top: e.pageY
                    });
                }
            }
        });
        $("body").bind("contextmenu", function(e) {
            e.preventDefault();
        });
    });
    function disConnect(){
        var param = $("#tree").tree('getSelected');
        $.post('disConnect?_token={{ csrf_token() }}', {id:param.id}, function(data) {
            $("#l_left").panel('refresh');
            $("#l_top").panel('refresh');
        });
    }
    function openTable(){
        var param = $("#tree").tree('getSelected');
        $('#tt').tabs('add',{
            title: param.text,
            href: 'l/middle/table?where='+param.id,
            closable: true
        });
    }
</script>