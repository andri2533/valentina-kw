<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HandleController@index');
Route::post('addServer', 'HandleController@addServer');
Route::get('getServer', 'HandleController@getServer');
Route::post('removeServer', 'HandleController@removeServer');
Route::post('connectToServer', 'HandleController@connectToServer');
Route::post('disConnect', 'HandleController@disConnect');

Route::get('l/{l}', 'HandleController@layout');
Route::post('l/{l}/tree', 'HandleController@tree');
Route::get('l/{l}/tree', 'HandleController@tree');
Route::get('l/{l}/table', 'HandleController@table');