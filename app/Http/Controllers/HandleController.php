<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Services\Dm;
use Illuminate\Http\Request;
use App\Models\Servers;
use Input;
use DB;

class HandleController extends Controller
{
	public function index(){
		return view('welcome');
		$him = ['a','b','c','A','B','C'];
		$result = array();
		foreach ($him as $h1) {
			foreach ($him as $h2) {
				foreach ($him as $h3) {
					$result[] = $h1.$h2.$h3;
				}
			}
		}
	}
	public function addServer(){
		$server = NULL;
		if(Input::get('id')!=NULL){
			$server = Servers::find(Input::get('id'));
			$server->update(Input::all());
		}else{
			$server = Servers::create(Input::all());
		}
		return response()->json($server);
	}
	public function getServer(){
		$servers = Servers::all()->map(function($v){
			$sr = '<a href="javascript:void(0)" id="mb" class="easyui-menubutton" 
        data-options="iconCls:\'icon-server\',hasDownArrow:false"><b>'.$v->nama.'</b></a>';
			return $sr;
		})->implode('&nbsp;');
		return $servers;
	}
	public function removeServer(){
		$server = Servers::find(Input::get('id'));
		$server->delete();
		return 1;
	}
	public function connectToServer(Request $req){
		$ck = Servers::find(Input::get('id'));
		if($ck){
			$ck->database = $ck->database();
			if ($req->session()->has('connected')) {
				$req->session()->push('connected', $ck);
			}else{
				$req->session()->put('connected', [$ck]);
			}
			return $req->session()->get('connected');
			// $db = new Dm($ck);
			// dd($db->query('show databases'));
		}else{
			return NULL;
		}
	}
	public function layout($l, Request $req){
		$result = NULL;
		switch ($l) {
			case 'top':
				$ses = collect($req->session()->get('connected'));
				$result = Servers::all()->map(function($v) use ($req, $ses){
					$v->active = FALSE;
					foreach($ses AS $ac){
						if($ac->id == $v->id){
							$v->active = TRUE;
						}
					}
					return $v;
				});
				break;
			case 'left':
				break;
			default:
				# code...
				break;
		}
		
		return view($l, compact('result'));
	}
	public function tree(Request $req){
		$result = $req->session()->get('connected');
		if($result==null) return '';
		// dd($req->where);
		if($req->where){
			$db = explode('.', $req->id);
			$server = Servers::find($db[0]);
			$cn = new Dm($server, $db[1]);
			$result = collect($cn->showTables())->map(function($v) use ($db){
				$v = (array) $v;
				$tbName = $v['Tables_in_'.$db[1]];
				$t['id']       = $db[0].".".$db[1].".".$tbName;
				$t['iconCls']  = 'fa fa-table';
				// $t['state']    = 'closed';
				$t['text']     = $tbName;
				$t['type']     = 'table';
				return $t;
			})->toArray();
		}else{
			$result = collect($result)->map(function($s, $si){
				$t = array();
				$t['id']       = $s->id;
				$t['iconCls']  = 'icon-server';
				$t['state']    = 'open';
				$t['text']     = $s->nama;
				$t['type']     = 'server';
				$t['children'] = $s->database->map(function($d, $di) use ($s, $si){
					$t['id']       = $s->id.".".$d->db;
					$t['iconCls']  = 'icon-database';
					$t['state']    = 'closed';
					$t['text']     = $d->db;
					$t['type']     = 'db';
					return $t;
				})->values()->toArray();
				return $t;
			})->values()->toArray();
		}

		return json_encode($result);
	}
	public function disConnect(Request $r){
		$ses = collect($r->session()->get('connected'))->map(function($v) use ($r){
			if($v->id == $r->id) return null;
			return $v;
		})->filter();
		if($ses->count()>0){
			$r->session()->put('connected', $ses);
		}else{
			$r->session()->forget('connected');
			$r->session()->flush();
		}
		return 1;
	}
	public function table(Request $r){
		$db      = explode('.', $r->from);
		$server  = Servers::find($db[0]);
		$cn      = new Dm($server, $db[1]);
		$columns = $cn->query("SHOW COLUMNS FROM ".$db[2]);
		if($r->json!=1){
			$columns = collect($columns)->map(function($c){
				$type = explode('(', $c->Type)[0];
				switch ($type) {
					case 'int':
						$c->eu_editor = "'numberbox'";
						break;
					case 'text':
						$c->eu_editor = "{type:'textarea',options:{height:100}}";
						break;
					
					default:
						$c->eu_editor = "{type:'text'}";
						break;
				}
				return $c;
			});
			// dd($columns);
			return view('table', compact('columns','db'));
		}else{
			$end = $r->rows;
			$start = ($r->page*$end)-($end);
			
			$orderBy = collect($columns)->map(function($v) use ($r){
				if($r->sort==$v->Field) return ' Order By '.$v->Field.' '.$r->order;
			})->filter()->values()[0];
			
			$limit = " LIMIT $start, $end";
			$sql = "SELECT * FROM ".$db[2].$orderBy.$limit;

			$row = collect($cn->query($sql))->map(function($v) use ($columns){
				$v = (array) $v;
				foreach ($columns as $co) {
					// if($v[$co->Field]==NULL AND $v[$co->Field]!="") $v[$co->Field] = '<NULL>';
				}
				return $v;
			})->toArray();
			// dd($row);

			$total = $cn->query("SELECT COUNT(*) AS total FROM ".$db[2]);
			
			$rt['total'] = $total[0]->total;
			$rt['rows'] = $row;
			return json_encode($rt);
		}
	}
}
