<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use App\Models\Servers;
use Input;
use DB;

class Dm extends Controller{

	public $con = null;

	public function __construct(Servers $serverss, $db = ''){
		$r =Input::all();
		$db = explode('.', $r->id);
		$database = @$db[1];
		dd($database);
		$server = Servers::find($db[0]);
		$cn = strtolower(str_replace(' ', '_', $server->nama)).$server->id;
		$dn = config('database.connections.mysql');
		$dn['host'] = $server->hosts;
		$dn['user'] = $server->users;
		$dn['password'] = $server->password;
		$dn['database'] = $database;
		config()->set('database.connections.'.$cn, $dn);
		$this->con = DB::connection($cn);
	}

	public function connect(){
		return $this->con;
	}
	public function showDatabases(){
		return $this->con->select('show databases');
	}
	public function query($query){
		return $this->con->select($query);
	}
	public function showTables(){
		return $this->con->select('show tables');
	}
}
