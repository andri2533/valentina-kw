<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Services\Dm;

class Servers extends Model
{
    protected $table    = 'servers';
    protected $fillable = ['nama', 'hosts', 'users', 'password', 'connection_type'];
    public $timestamps  = TRUE;

    public function database(){
    	$rs = new Dm($this);
    	return collect($rs->showDatabases())->map(function($v){
    		return (Object) ['db'=>$v->Database];
    	})->sortBy('db');
    }

}
